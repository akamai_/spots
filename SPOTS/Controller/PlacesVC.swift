//
//  PlacesVC.swift
//  SPOTS
//
//  Created by Robert Szost on 14.01.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import UIKit

class PlacesVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    //Outlet for collection view to fill with data (places)
    @IBOutlet weak var placesCollection: UICollectionView!

    //List of places by category
    public var places = [Place]()
    //Variable used to long time-press gesture
    var longPressGesture: UILongPressGestureRecognizer!

    //Loading view to memory
    override func viewDidLoad() {
        super.viewDidLoad()

        placesCollection.dataSource = self
        placesCollection.delegate = self

        //Gesture to reorder items
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
        placesCollection.addGestureRecognizer(longPressGesture)
    }
    
    //Places initialisation
    func initPlaces(category: Category) {
        places = DataService.instance.getPlaces(forCategoryTitle: category.title)
        navigationItem.title = category.title
    }

    //Calculates numer of items
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return places.count
    }

    //Presenting places to user
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaceCell", for: indexPath) as? PlaceCell {
            let place = places[indexPath.row]
            cell.updateViews(place: place)
            return cell
        }
        return PlaceCell()
    }

    //Enabling reordering
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }

    //Switching places
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
            let tempStorageForPlace = places[destinationIndexPath.row]
            places[destinationIndexPath.row] = places[sourceIndexPath.row]
            places[sourceIndexPath.row] = tempStorageForPlace

    }
    
    //Gesture handling
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        switch (gesture.state) {
        case .began:
            guard let selectedIndexPath = placesCollection.indexPathForItem(at: gesture.location(in: placesCollection)) else {
                break
            }
            placesCollection.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            placesCollection.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            placesCollection.endInteractiveMovement()
        default:
            placesCollection.cancelInteractiveMovement()
        }
    }
}
