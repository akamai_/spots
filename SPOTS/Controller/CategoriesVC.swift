//
//  ViewController.swift
//  SPOTS
//
//  Created by Robert Szost on 07.01.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import UIKit

class CategoriesVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //Outlet for table view to fill with data (category)
    @IBOutlet weak var categoryTable: UITableView!

    //Loading view to memory
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryTable.dataSource = self
        categoryTable.delegate = self
    }

    //Counting how much row is needed
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getCategories().count
    }

    //Getting categories at correct row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as? CategoryCell {
            let category = DataService.instance.getCategories()[indexPath.row]
            cell.updateViews(category: category)
            return cell
        } else {
            return CategoryCell()
        }
    }

    //Finding which row was selected to list correct places for choosen category
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = DataService.instance.getCategories()[indexPath.row]
        performSegue(withIdentifier: "PlacesVC", sender: category)
    }
    
    //Function preparing data for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let placesVC = segue.destination as? PlacesVC {
            let barButton = UIBarButtonItem()
            barButton.title = ""
            navigationItem.backBarButtonItem = barButton
            assert(sender as? Category != nil)
            placesVC.initPlaces(category: sender as! Category)
        }
    }
}

