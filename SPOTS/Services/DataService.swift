//
//  DataService.swift
//  SPOTS
//
//  Created by Robert Szost on 08.01.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import Foundation

class DataService {
    static let instance = DataService()

    //Define categories
    private let categories = [
        Category(title: "BARS", imageName: "barCategory.jpg"),
        Category(title: "CAFES", imageName: "coffeeCategory.jpg"),
        Category(title: "MONUMENTS", imageName: "monumentCategory.jpg"),
        Category(title: "PARTIES", imageName: "partyCategory.jpg"),
        Category(title: "RESTAURANTS", imageName: "restaurantCategory.jpg"),
        Category(title: "STORES", imageName: "storeCategory.jpg"),
    ]

    //Hardcoded content, arrays with places spited by categories
    private let bars = [
        Place(title: "Pattys", imageName: "bar1.jpg", description: "Cheap beer, not so crowded, nice barmaids"),
        Place(title: "Ace Bar", imageName: "bar2.jpg", description: "Expensive place, open till 4am, a lots of famous people"),
        Place(title: "Quantum", imageName: "bar3.jpg", description: "Open all night, lots of different drinks"),
        Place(title: "Wonka", imageName: "bar4.jpg", description: "Bartender tells stories about chocolate factory"),
    ]

    private let cafes = [
        Place(title: "Coffee Time", imageName: "coffee1.jpg", description: "Great coffee, nice views"),
        Place(title: "Nomono", imageName: "coffee2.jpg", description: "Home atmosphere, lots of syrups"),
        Place(title: "Costa", imageName: "coffee3.jpg", description: "Great localisation, hipster place"),
        Place(title: "Screen coffee", imageName: "coffee4.jpg", description: "Near work, friendly atmosphere"),
    ]

    private let monuments = [
        Place(title: "Statue of Liberty", imageName: "monument1.jpg", description: "Great place for date"),
        Place(title: "Monument Valley", imageName: "monument2.jpg", description: "Lots of free space, kids love it"),
        Place(title: "Pakistan Monument", imageName: "monument3.jpg", description: "From air the monument looks like a star, popular picnic destination"),
        Place(title: "Porte du Peyrou", imageName: "monument4.jpg", description: "Magnificent arch, marks the entrance to the historic center, very beautiful"),
    ]

    private let parties = [
        Place(title: "Laserogy", imageName: "party1.jpg", description: "Lots of lasers, free entry, most of music is trance"),
        Place(title: "Grand-mas", imageName: "party2.jpg", description: "Good food, folk music, mostly old peoples"),
        Place(title: "OutDoor", imageName: "party3.jpg", description: "Outdoor party, rock music, no chairs"),
        Place(title: "Acid", imageName: "party4.jpg", description: "Loud music, lots of people, always crowded"),
    ]

    private let restaurants = [
        Place(title: "Road American", imageName: "restaurant1.jpg", description: "Fast service, cheap, live music"),
        Place(title: "TepanTerryiaki", imageName: "restaurant2.jpg", description: "Good place for date, not so crowded, need reservation"),
        Place(title: "Food Gallery", imageName: "restaurant3.jpg", description: "Spectacular view, comfy armchairs"),
        Place(title: "Biquit", imageName: "restaurant4.jpg", description: "Very expensive, nice place for flexing"),
    ]

    private let stores = [
        Place(title: "Walmart", imageName: "store1.jpg", description: "Great for big shopping, can buy here almost everything"),
        Place(title: "ThinkGeek", imageName: "store2.jpg", description: "Cameras, drones, computers, accessories"),
        Place(title: "Forever21", imageName: "store3.jpg", description: "Nice clothes, low prices"),
        Place(title: "Computers", imageName: "store4.jpg", description: "Nerds place, a lots of assortiment"),

    ]

    //Function to get all categories
    func getCategories() -> [Category] {
        return categories
    }

    //Function which recognise which category was choosen indicated by segue
    func getPlaces(forCategoryTitle title: String) -> [Place] {
        switch title {
        case "BARS":
            return getBars()
        case "MONUMENTS":
            return getMonuments()
        case "PARTIES":
            return getParties()
        case "RESTAURANTS":
            return getRestaurants()
        case "STORES":
            return getStores()
        case "CAFES":
            return getCafes()
        default:
            return getBars()
        }
    }

    //Functions to return choosen array with places by categories indicated by switch in getPlaces
    func getBars() -> [Place] {
        return bars
    }

    func getMonuments() -> [Place] {
        return monuments
    }

    func getParties() -> [Place] {
        return parties
    }

    func getRestaurants() -> [Place] {
        return restaurants
    }

    func getCafes() -> [Place] {
        return cafes
    }

    func getStores() -> [Place] {
        return stores
    }
}
