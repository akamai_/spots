//
//  Category.swift
//  SPOTS
//
//  Created by Robert Szost on 07.01.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import Foundation

struct Category {

    //Properties of model
    private(set) var title: String!
    private(set) var imageName: String!

    //Initialisation
    init(title: String, imageName: String) {
        self.title = title
        self.imageName = imageName
    }
}
