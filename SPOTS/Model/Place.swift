//
//  Place.swift
//  SPOTS
//
//  Created by Robert Szost on 13.01.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import Foundation

struct Place {

    //Properties of model
    private(set) var title: String!
    private(set) var imageName: String!
    private(set) var description: String!

    //Initialisation
    init(title: String, imageName: String, description: String) {
        self.title = title
        self.imageName = imageName
        self.description = description
    }
}
