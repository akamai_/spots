//
//  CategoryCell.swift
//  SPOTS
//
//  Created by Robert Szost on 07.01.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    //Outlets placed at first view controller on Main.storyboard
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!

    //Function which update screen
    func updateViews(category: Category) {
        categoryImage.image = UIImage(named: category.imageName)
        categoryTitle.text = category.title
    }

}
