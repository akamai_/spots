//
//  PlaceCell.swift
//  SPOTS
//
//  Created by Robert Szost on 13.01.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import UIKit

class PlaceCell: UICollectionViewCell {

    //Outlets placed at second view controller on Main.storyboard (after choosing categories)
    @IBOutlet weak var placeTitle: UILabel!
    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var placeDescription: UILabel!

    //Function which update screen
    func updateViews(place: Place) {
        placeImage.image = UIImage(named: place.imageName)
        placeTitle.text = place.title
        placeDescription.text = place.description

    }
}
